console.log('Loading background.js');

function highlightResults(results) {
  console.log(133, results);
  if (results.count > 0) {
    browser.find.highlightResults();
  }
}

function processMessage(message) {
  console.log(10, message.selectedText);
  console.log(11, browser.browsingContext);

  switch (message.action) {
    case "highlight":
      if (message.selectedText) {
        browser.find
          .find(message.selectedText, {
            caseSensitive: false,
            includeRectData: false,
            includeRangeData: false
          })
          .then(highlightResults)
          .catch(e => console.error("!!", e));
      }
      break;
  }
}

console.log(111);
browser.runtime.onMessage.addListener(processMessage);
