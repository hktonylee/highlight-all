let lastSelectedText = "";

document.addEventListener("mouseup", evt => {
  if (lastSelectedText) {
    console.log("mouseup --", lastSelectedText);
    browser.runtime.sendMessage({
      action: "highlight",
      selectedText: lastSelectedText
    });
  }
});

document.addEventListener("selectionchange", evt => {
  // console.log('selectionchange', evt);

  const selection = document.getSelection();
  if (selection.isCollapsed) {
    lastSelectedText = "";
  } else {
    const selectedText = selection.toString();
    lastSelectedText = selectedText;
  }
});
